<?php

namespace App\Repositories\DataProviderX;

use App\Models\DataProviderX;
use App\Repositories\CommonRepository;
use Illuminate\Support\Facades\DB;

class DataProviderRepository extends CommonRepository implements DataProviderRepositoryInterface
{

    public function filterColumns()
    {
        return [
           $this->amountTransactionBetween('transactionAmount_min'),
           $this->amountTransactionBetween('transactionAmount_max'),
           'currency',
           'senderPhone',
           'transactionStatus',
           'transactionDate',
           'transactionIdentification',
           'created_at',
           'phoneY._id',
           $this->amountYBetween('amountY_min'),
           $this->amountYBetween('amountY_max'),
           'phoneY.currency',
           'phoneY.phone',
           'phoneY.status',
           'phoneY.created_at',
           'phoneW.id',
           $this->amountWBetween('amountW_min'),
           $this->amountWBetween('amountW_max'),
           'phoneW.currency',
           'phoneW.phone',
           'phoneW.status',
           'phoneW.created_at'
        ];
    }

    public function model()
    {
        return DataProviderX::class;
    }

    public function index(){
        $transactions = $this->setFilters()->with(['phoneY', 'phoneW'])->get();

        return $transactions;
    }

}
