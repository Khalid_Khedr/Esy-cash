<?php

namespace App\Repositories\DataProviderX;

use App\Repositories\CommonRepositoryInterface;

interface DataProviderRepositoryInterface extends CommonRepositoryInterface
{
    public function index();
}
