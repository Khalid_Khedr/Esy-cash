<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryCriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

interface CommonRepositoryInterface extends RepositoryInterface, RepositoryCriteriaInterface
{
    public function setFilters();
}
