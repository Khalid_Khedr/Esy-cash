<?php

namespace App\Repositories;

use App\Foundation\Classes\FilterTransactionAmountBetween;
use App\Foundation\Classes\FilterAmountYBetween;
use App\Foundation\Classes\FilterAmountWBetween;
use Prettus\Repository\Eloquent\BaseRepository;
use Spatie\QueryBuilder\QueryBuilder;
use App\Foundation\Traits\DeactivatedTrait;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;

class CommonRepository extends BaseRepository implements CommonRepositoryInterface
{

    public function setFilters()
    {
        return QueryBuilder::for($this->getModel())->allowedFilters($this->filterColumns());
    }

    protected function filterColumns()
    {

    }

    public function amountTransactionBetween($value)
    {
        return AllowedFilter::custom($value, new FilterTransactionAmountBetween);
    }

    public function amountYBetween($value)
    {
        return AllowedFilter::custom($value, new FilterAmountYBetween());
    }

    public function amountWBetween($value)
    {
        return AllowedFilter::custom($value, new FilterAmountWBetween());
    }

    public function model()
    {
        // TODO: Implement model() method.
    }

}
