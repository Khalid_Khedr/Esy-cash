<?php

namespace App\Foundation\Classes;

use Carbon\Carbon;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterAmountYBetween implements Filter
{
    public function __invoke(Builder $query, $value ,  string $property)
    {
        $operator = '>=' ;
        if($property == 'amountY_max')
        {
            $operator = '<=' ;
        }
        $property = 'amount' ;
        $query->whereHas('phoneY', function($q) use($property, $operator, $value){
            $q->where($property,$operator,$value);
        });
    }
}


