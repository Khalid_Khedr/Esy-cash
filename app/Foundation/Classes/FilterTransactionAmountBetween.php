<?php

namespace App\Foundation\Classes;

use Carbon\Carbon;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterTransactionAmountBetween implements Filter
{
    public function __invoke(Builder $query, $value ,  string $property)
    {
        $operator = '>=' ;
        if($property == 'transactionAmount_max')
        {
            $operator = '<=' ;
        }
        $property = 'transactionAmount' ;
        $query->where($property,$operator,$value);
    }
}


