<?php

namespace App\Foundation\Classes;

use Carbon\Carbon;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FilterAmountWBetween implements Filter
{
    public function __invoke(Builder $query, $value ,  string $property)
    {
        $operator = '>=' ;
        if($property == 'amountW_max')
        {
            $operator = '<=' ;
        }
        $property = 'amount' ;
        $query->whereHas('phoneW', function($q) use($property, $operator, $value){
            $q->where($property,$operator,$value);
        });
    }
}


