<?php

namespace App\Foundation\Traits;

trait ApiResponseTrait
{
    public function successResponse(mixed $data = null, bool $status = true, string $message = '', int $code = 200, array $additional = [], array $headers = [])
    {
        return $this->apiResource($data, $status, $message, $code, $headers);
    }

    public function apiResource(mixed $data = null, bool $status = true, string $message = '', int $code = 200, array $additional = [], array $headers = [])
    {
        $response = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ] + $additional;
        return response()->json($response, $code, $headers);
    }

    public function errorResponse(mixed $data = null, bool $status = false, string $message = '', int $code = 422, array $headers = [])
    {
        return $this->apiResource(status: $status, message: $message, code: $code, additional: ['errors' => $data], headers: $headers);
    }

}
