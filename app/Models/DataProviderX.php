<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataProviderX extends Model
{
    protected $guarded = [];

    public function phoneY(){
        return $this->belongsTo(DataProviderY::class, 'Phone_y_id');
    }

    public function phoneW(){
        return $this->belongsTo(DataProviderW::class, 'Phone_w_id');
    }
}
