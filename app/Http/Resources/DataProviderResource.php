<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DataProviderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
             'transactionAmount' => $this->transactionAmount,
             'currency' => $this->currency,
             'senderPhone' => $this->senderPhone,
             'transactionStatus' => $this->transactionStatus,
             'transactionDate' => $this->transactionDate,
             'transactionIdentification' => $this->transactionIdentification,
             'transactionAmount' => $this->transactionAmount,
             'DataProviderY' => DataProviderYResource::make($this->whenLoaded('phoneY')),
             'DataProviderW' => DataProviderWResource::make($this->whenLoaded('phoneW')),
        ];
    }
}
