<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DataProviderX\DataProviderRepositoryInterface;
use App\Foundation\Traits\ApiResponseTrait;
use App\Http\Resources\DataProviderResource;

class DataProviderController extends Controller
{
    use ApiResponseTrait;

    private $repositery;

    public function __construct(DataProviderRepositoryInterface $repositery)
    {
        $this->repositery = $repositery;
    }

    public function index(){
        $transactions = $this->repositery->index();
        return $this->successResponse(DataProviderResource::collection($transactions));
    }
}
