<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_provider_x_e_s', function (Blueprint $table) {
            $table->double('transactionAmount');
            $table->string('currency', 3);
            $table->string('senderPhone');
            $table->enum('transactionStatus', ['1', '2', '3']);
            $table->dateTime('transactionDate');
            $table->text('transactionIdentification');
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
            $table->foreignId('Phone_y_id')->constrained('data_provider_y_s')->nullable()->OnDelete('no action');
            $table->foreignId('Phone_w_id')->constrained('data_provider_w_s')->nullable()->OnDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_provider_x_e_s');
    }
};
