<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\DataProviderW;

class DataProviderWSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DataProviderW::Create([
            "id" => 12345678,
            "amount" => 500.00,
            "currency" => 'EGP',
            "phone" => '00201134567890',
            "status" => 'done',
            "created_at" => '2021-03-29 09:36:11',
        ]);
    }
}
