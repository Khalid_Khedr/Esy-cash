<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\DataProviderY;

class DataProviderYSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DataProviderY::Create([
            "amount" => 500.00,
            "currency" => 'EGP',
            "phone" => '00201134567890',
            "status" => '100',
            "created_at" => '2021-03-29 09:36:11',
            "_id" => '4fc2-a8d1'
        ]);
    }
}
