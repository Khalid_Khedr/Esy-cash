<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\DataProviderX;

class DataProviderXSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DataProviderX::Create([
            "transactionAmount" => 200,
            "currency" => 'USD',
            "senderPhone" => '00201134567890',
            "transactionStatus" => '1',
            "transactionDate" => '2021-03-29 09:36:11',
            "transactionIdentification" => 'd3d29d70-1d25-11e3-8591-034165a3a613',
            "Phone_w_id" => 12345678,
            "Phone_y_id" => 1,
        ]);
    }
}
